# Introduction

This is a simple end to end data processing application which performs streaming analytics and lets you know the top 
words in a tweet stream in mini batches of 10 seconds. The complete application has 3 main components. 

- A Kafka kubernetes cluster with brokers and zookeeper configurations done using Helm
- A Kafka producer client which collects tweets from Twitter API and sends the messages to a Kafka topic
- A simple Pyspark Streaming application written in Jypyter notebook


# Prerequisites
To be able to reproduce the app locally, you would need the following prerequisites.

- [Python 3.7](https://www.python.org/downloads/release/python-370/): To run the Tweet producer client
- [Virtualenv](https://virtualenv.pypa.io/en/latest/): To create Python virtual environments for managing 
python libraries
- [Minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/): To effortlessly start Kubernetes 
cluster locally
- [Helm](https://helm.sh/): Kubernetes package manager which makes it very simple to manage Kubernetes applications
- [Spark](https://spark.apache.org/): Which provides the most popular large scale data processing framework

# Application Components
In this section, I will motivate my reasoning behind the design choices for building various components 
of this data processing App.

## Kafka Kubernetes cluster
The container community has come a long way since Docker was invented. Containers provide a great way to 
build, ship and deploy portable code to different deployment environments taking out the risk of diversity of these 
environments. Kubernetes is a orchestrator which lets you manage the containers and also takes care of 
networking and scaling the applications. The reason behind choosing Kubernetes for this use case is that, the Kubernetes
cluster that you build and where you test your applications, can be easily deployed and managed in 
any of the Cloud provides or even on premises. That gives you the power to take advantage of the cloud and build 
highly scalable and available applications. For eample, Just by deploying this Kubernetes Kafka cluster in the cloud 
and changing a few paramenter you can add additional capacity to handle higher load from producers/consumers.

I have also used Helm, which is a package manager for Kubernetes and makes Kubernetes deployments even easier with 
simple YAML configuration files. Its very easy to iteratively make changes to the Kubernetes cluster using Helm. In this 
application, we have specified all the Kafka related configurations inside the file **kafka.yaml**. 

### Instructions
- First we start a **minikube** Kubernetes cluster locally using the command <pre><code>minikube start</code></pre>
- Once the cluster is up and running, we deploy the Kafka chart in the cluster. <pre><code>helm repo add incubator https://kubernetes-charts-incubator.storage.googleapis.com/
helm install -f kafka.yaml incubator/kafka --generate-name</code></pre>
 
Now you have a Kafka Cluster running with the Broker and Zookeeper pods as per the configurations in the **kafka.yaml**. 

## Kafka producer client
Next step is to build a Kafka producer client to send the tweets to a Kafka topic endpoint. I have chosen to use Python 
for this. But, it could also have been written in other Programming languages like Java/Scala. To get the twitter stream,
first you have to create a Twitter developer account and [register an application](https://developer.twitter.com/).
After registering the application, we can get the 4 credentials needed by the **tweepy** Python library to be able to get
twitter stream data. Please fill the credentials in the file **kafka_consumer.yaml**. 

<pre><code>
consumer_key = 'YOUR CREDENTIALS'
consumer_secret = 'YOUR CREDENTIALS'
access_token = 'YOUR CREDENTIALS'
access_token_secret = 'YOUR CREDENTIALS'
</code></pre>

Also have a look at your Kafka broker list and replace the values if needed in the file **kafka_consumer.yaml**. 

<pre><code>
kafka_endpoint = ['kafka.cluster.local:31090', 'kafka.cluster.local:31091', 'kafka.cluster.local:31092']
</code></pre>

### Instructions
- For the Tweet producer client, we create and activate a virtual environment <pre><code>virtualenv --python=python3.7 venv && source venv/bin/activate</code></pre>
- We install the Python library requirements <pre><code>pip install -r requirements.txt</code></pre>
- Finally we start the producer to start sending tweets to Kafka Topic.<pre><code>python tweet_producer.py</code></pre>

## Pyspark Streaming application
Spark is a multi-purpose big data processing framework which provides both batch and stream processing capabilities.
The reason for choosing Spark for the analysis here is that all the major cloud providers provice Spark framework as 
a service (EMR in AWS, Dataproc in GCP). Therefore, the code here can be easily deployed to such managed clusters to 
handle any amount/speed of incoming streaming data as needed. **Jupyter noteboook** provides a way to create notebooks to perform 
rapid prototyping. Its a great way to develop POCs and share code with your colleagues with your comments. Same code 
can be converted from notebooks as python files and submitted as Pyspark applications.

### Instructions
- After Installing Spark, we will need to set up some environment variables so that Pyspark uses Jupyter notebook
<pre><code>export PYSPARK_DRIVER_PYTHON=jupyter
export PYSPARK_DRIVER_PYTHON_OPTS='notebook'</code></pre>
- Make sure all the jars are available for Spark. In this case, we need "spark-streaming-kafka-0-8-assembly_2.11-2.4.5.jar"
 to be able use Spark's Kafka streaming utilities.
- Start Pyrpark in local mode <pre><code>pyspark --jars jars/spark-streaming-kafka-0-8-assembly_2.11-2.4.5.jar</code></pre>
- Finally in the Spark application, we create a streaming context and perform some streaming analytics over a batch of 
10 seconds to find out the most popular words in the tweets over that 10 seconds period.

# Future Improvements
Using Kubernetes for this use case gives us a lot of flexibility to deploy our solution in any cloud providers and to scale when needed.
But it still requires a good understanding of the containers and orchestration technologies, deployments, registrys etc.
All the major cloud providers these days provide managed services to build similar applications. For example,
in Google cloud Platform, we can use streaming service Pubsub, Data processing frameworks like Dataflow and warehousing solutions 
like Bigquery to build end to end managed scalable data streaming applications with very less effort. Similar solutions are also provided
by AWS and Azure. Using such managed services takes away the administration overhead that is still present with Kubernetes.
Therefore I would lean more towards the cloud managed services for such use case unless the company has some policy that restricts them to 
make use of them.

# Demo
You can find a demo recording of the complete streaming application in the file **demo.mov**
