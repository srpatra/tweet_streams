from __future__ import absolute_import, print_function

# Import modules
import json
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from kafka import KafkaProducer

# Twitter Credentials
consumer_key = 'YOUR CREDENTIALS'
consumer_secret = 'YOUR CREDENTIALS'
access_token = 'YOUR CREDENTIALS'
access_token_secret = 'YOUR CREDENTIALS'
streaming_mode = 'normal'

# Kafka configuration
kafka_endpoint = ['kafka.cluster.local:31090', 'kafka.cluster.local:31091', 'kafka.cluster.local:31092']
kafka_topic = 'tweets'


class StdOutListener(StreamListener):
    """ A listener handles tweets that are received from the stream.
    This is a basic listener that just prints received tweets to stdout.

    """
    producer = KafkaProducer(bootstrap_servers=kafka_endpoint)

    def on_data(self, data):
        data_json = json.loads(data)
        if 'text' in data_json:
            str_tweet = data_json['text'].encode('utf-8')
            self.producer.send(kafka_topic, str_tweet)
            print(str_tweet)
        return True

    def on_error(self, status):
        print(status)


if __name__ == '__main__':
    l = StdOutListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    stream = Stream(auth, l)
    stream.filter(track=['corona'])