venv:
	virtualenv --python=python3.7 venv && source venv/bin/activate

rebuild_venv:
	rm -rf venv
	virtualenv --python=python3.7 venv && source venv/bin/activate

init:
	pip install -r requirements.txt